<?php
include('includes/connection.php');
?>
 
<?php include('includes/admin_header.php'); ?>


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					
					<div class="col-md-12 ">
						<div class="x_panel">
							<div class="x_title">
								<h2>Manage Products</h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
									
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<br />
								<form class="form-horizontal form-label-left">

									<div class="form-group row ">
										<label class="control-label col-md-3 col-sm-3 ">Product Name</label>
										<div class="col-md-9 col-sm-9 ">
											<input type="text" class="form-control" placeholder="">
										</div>
									</div>
									
									
									<div class="form-group row">
										<label class="control-label col-md-3 col-sm-3 ">Product Description <span class="required"></span>
										</label>
										<div class="col-md-9 col-sm-9 ">
											<textarea class="form-control" rows="3" placeholder=""></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-3 col-sm-3 ">Product Price</label>
										<div class="col-md-9 col-sm-9 ">
											<input type="text" class="form-control" value="">
										</div>
									</div>
									
									<div class="form-group row">
										<label class="control-label col-md-3 col-sm-3 ">Product Category</label>
										<div class="col-md-9 col-sm-9 ">
											<select class="form-control">
												<option>Choose Category</option>
												<option>Option one</option>
												<option>Option two</option>
												<option>Option three</option>
												<option>Option four</option>
											</select>
										</div>
									</div>
									<div class="row form-group">
<div class="col col-md-3">
<label for="file-input" class=" form-control-label">Product Image</label>
</div>
<div class="col-12 col-md-9">
<input type="file" id="file-input" name="file-input" class="form-control-file">
</div>
</div>

									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-9 col-sm-9  offset-md-3">
											<button type="button" class="btn btn-primary">Add</button>
											
										</div>
									</div>

								</form>
							</div>
							
						</div>
						
					</div>
	   
		</div>
		
		  <div class="clearfix"></div>

		  <div class="col-md-12 col-sm-12  ">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Products</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>

			  <div class="x_content">


				<div class="table-responsive">
				  <table class="table table-striped jambo_table bulk_action">
					<thead>
					  <tr class="headings">
						<th>
						  <input type="checkbox" id="check-all" class="flat">
						</th>
						<th class="column-title">Product ID</th>
						<th class="column-title">Product Name</th>
						<th class="column-title">Product Description</th>
						<th class="column-title">Product Price</th>
						<th class="column-title">Product Image</th>
						<th class="column-title">Category </th>
						  <th class="column-title no-link last"><span class="nobr">Edit</span>
						<th class="column-title no-link last"><span class="nobr">Delete</span>

						</th>
					
				  </table>
				</div>
						
					
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	  </div>
	  
	<!-- /page content -->

	<!-- footer content -->
	
	<!-- /footer content -->
  </div>
</div>





						</div>
					</div>

					
<?php include('includes/admin_footer.php'); ?>