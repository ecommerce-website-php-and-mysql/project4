<?php
include('includes/connection.php');
?>
 
<?php include('includes/admin_header.php'); ?>

			<!-- page content -->
			
					<div class="right_col" role="main">
						<div class="">
		
				<div class="col-md-12 ">
									<div class="x_panel">
										<div class="x_title">
											<h2>Manage Category</h2>
											<ul class="nav navbar-right panel_toolbox">
												<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
												</li>
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#">Settings 1</a>
														<a class="dropdown-item" href="#">Settings 2</a>
													</div>
												</li>
												<li><a class="close-link"><i class="fa fa-close"></i></a>
												</li>
											</ul>
											<div class="clearfix"></div>
										</div>
										<div class="x_content">
											<br />
											<form class="form-horizontal form-label-left">
		
												<div class="form-group row ">
													<label class="control-label col-md-3 col-sm-3 ">Category Name</label>
													<div class="col-md-9 col-sm-9 ">
														<input type="text" class="form-control" placeholder="">
													</div>
												</div>
												
												
												<div class="form-group row">
													<label class="control-label col-md-3 col-sm-3 ">Category Description <span class="required"></span>
													</label>
													<div class="col-md-9 col-sm-9 ">
														<textarea class="form-control" rows="3" placeholder=""></textarea>
													</div>
												</div>
												
		
												<div class="ln_solid"></div>
												<div class="form-group">
													<div class="col-md-9 col-sm-9  offset-md-3">
														<button type="button" class="btn btn-primary">Add</button>
														
													</div>
												</div>
		
											</form>
										</div>
										
									</div>
									
								</div>
				   
					</div>
					
					  <div class="clearfix"></div>
		
					  <div class="col-md-12 col-sm-12  ">
						<div class="x_panel">
						  <div class="x_title">
							<h2>Products</h2>
							<ul class="nav navbar-right panel_toolbox">
							  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							  </li>
							  <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Settings 1</a>
									<a class="dropdown-item" href="#">Settings 2</a>
								  </div>
							  </li>
							  <li><a class="close-link"><i class="fa fa-close"></i></a>
							  </li>
							</ul>
							<div class="clearfix"></div>
						  </div>
		
						  <div class="x_content">
		
		
							<div class="table-responsive">
							  <table class="table table-striped jambo_table bulk_action">
								<thead>
								  <tr class="headings">
									<th>
									  <input type="checkbox" id="check-all" class="flat">
									</th>
									<th class="column-title">Category ID</th>
									<th class="column-title">Category Name</th>
									<th class="column-title">Category Description</th>
									  <th class="column-title no-link last"><span class="nobr">Edit</span>
									<th class="column-title no-link last"><span class="nobr">Delete</span>
		
									</th>
								
							  </table>
							</div>
									
								
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				  </div>
				  
				<!-- /page content -->
		
				<!-- footer content -->
				
				<!-- /footer content -->
			  </div>
			</div>

					

<?php include('includes/admin_footer.php'); ?>